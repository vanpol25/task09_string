package com.polahniuk.model.big_task;

/**
 * Class has letter from word class {@link Word}.
 */
public class Letter {

    private String letter;

    private Letter(String letter) {
        this.letter = letter;
    }

    public static Letter createLetter(String letter) {
        return new Letter(letter);
    }

    public String getLetter() {
        return letter;
    }
}
