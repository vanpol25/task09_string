package com.polahniuk.model.big_task;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class has word from {@link Sentence}.
 * Also create letters of a word.
 */
public class Word {

    private String word;

    private List<Letter> letters = new ArrayList<>();

    private Word(String word) {
        this.word = word;
        createListOfLetters();
    }

    public static Word createWord(String sentence) {
        return new Word(sentence);
    }

    public String getWord() {
        return word;
    }

    public List<Letter> getLetters() {
        return letters;
    }

    /**
     * Create list of letters.
     */
    private void createListOfLetters() {
        Pattern pattern = Pattern.compile("[A-Za-z0-9`]");
        Matcher matcher = pattern.matcher(word);
        while (matcher.find()) {
            letters.add(Letter.createLetter(matcher.group()));
        }
    }

    @Override
    public String toString() {
        return word;
    }
}