package com.polahniuk.model.big_task;

import java.util.ArrayList;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class has text from formatted from file.
 * Also create sentences of a {@link Text#text}.
 */
public class Text {

    private String text;

    private List<Sentence> sentences = new ArrayList<>();

    private Text(String text) {
        this.text = formatText(text);
        createListOfSentences();
    }

    public static Text createText(String text) throws NullPointerException {
        if (text == null) {
            throw new NullPointerException();
        }
        return new Text(text);
    }

    private int size() {
        return sentences.size();
    }

    public String getText() {
        return text;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    /**
     * Format the text from file due to RegEx.
     * Formatting text to replace tabs and spaces with one space.
     * @param text - non formatted text.
     * @return - formatted text.
     */
    private String formatText(String text) {
        Pattern pattern = Pattern.compile("[\\t\\n\\r\\f\\s]");
        Matcher matcher = pattern.matcher(text);
        String formatted;
        if (matcher.find()) {
            formatted = matcher.replaceAll(" ");
            pattern = Pattern.compile("\\s\\s+");
            matcher = pattern.matcher(formatted);
            if (matcher.find()) {
                formatted = matcher.replaceAll(" ");
            }
        } else {
            formatted = text;
        }
        return formatted;
    }

    /**
     * Create list of sequences.
     */
    private void createListOfSentences() {
        Pattern pattern = Pattern.compile("[\\w].+?\\.");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            sentences.add(Sentence.createSentence(matcher.group()));
        }
    }
}
