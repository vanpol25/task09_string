package com.polahniuk.model.big_task;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class has sentence from {@link Text}.
 * Also create words of a sentence.
 */
public class Sentence implements Comparable {

    private String sentence;

    private List<Word> words = new ArrayList<>();

    private Sentence(String sentence) {
        this.sentence = sentence;
        createListOfWords();
    }

    public static Sentence createSentence(String sentence) {
        return new Sentence(sentence);
    }

    public String getSentence() {
        return sentence;
    }

    public List<Word> getWords() {
        return words;
    }

    /**
     * Create list of words.
     */
    private void createListOfWords() {
        Pattern pattern = Pattern.compile("\\b[A-Za-z0-9]\\w*");
        Matcher matcher = pattern.matcher(sentence);
        while (matcher.find()) {
            words.add(Word.createWord(matcher.group()));
        }
    }

    /**
     * @param o - method compareTo from {@link Comparable}.
     * @return - 1, 0 or -1.
     */
    @Override
    public int compareTo(Object o) {
        return sentence.compareTo(o.toString());
    }

    @Override
    public String toString() {
        return sentence;
    }
}
