package com.polahniuk.model.big_task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class has all business logic of BigTask project.
 */
public class BigTask {
    /**
     * Main {@link Text} build in this class.
     */
    private Text text;
    /**
     * This String created by reading file text.txt.
     */
    private String textFromFile;

    /**
     * @param reference - create {@link BigTask#textFromFile} by this reference.
     * @throws IOException - while reading file problems.
     */
    public BigTask(String reference) throws IOException {
        textFromFile = new String(Files.readAllBytes(Paths.get(reference)));
        text = Text.createText(textFromFile);
    }

    /**
     * @return - non formatted from file.
     */
    public String getText() {
        return textFromFile;
    }

    /**
     * @return - the largest number of text sentences that have the same words.
     */
    public List<Map.Entry<String, Integer>> firstTask() {
        Set<String> setWords = getWordsSet();
        Map<String, Integer> mapWords = new HashMap<>();
        for (String str : setWords) {
            for (Sentence sen : text.getSentences()) {
                if (sen.getSentence().matches("(.*\\s)" + str + "(\\s.*)")) {
                    mapWords.merge(str, 1, (oldV, newV) -> oldV + newV);
                }
            }
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<>(mapWords.entrySet());
        list.sort(Map.Entry.comparingByValue());
        return list;
    }

    /**
     * @return - all sentences of the given text in order of increasing the number of words in
     * to each of them.
     */
    public List<Map.Entry<Sentence, Integer>> secondTask() {
        Map<Sentence, Integer> mapWordsCount = new HashMap<>();
        for (Sentence sen : text.getSentences()) {
            mapWordsCount.put(sen, sen.getWords().size());
        }
        List<Map.Entry<Sentence, Integer>> list = new ArrayList<>(mapWordsCount.entrySet());
        list.sort(Map.Entry.comparingByValue());
        return list;
    }

    /**
     * @return - such a word in the first sentence, which is not present in any of the others
     * sentence.
     */
    public String thirdTask() {
        List<Sentence> senList = text.getSentences();
        List<String> words = senList.get(0).getWords().stream().map(Word::toString).collect(Collectors.toList());
        senList.remove(0);
        StringBuilder text = new StringBuilder();
        for (Sentence sen : senList) {
            text.append(sen.toString());
        }
        for (String word : words) {
            if (!text.toString().matches("(.*\\s)" + word.toLowerCase() + "(\\s.*)")) {
                return word;
            }
        }
        return null;
    }

    /**
     * @param length - given length.
     * @return - text in all the sentences of the text without repetition
     * words of a given length.
     */
    public Set<String> fourthTask(int length) {
        String fullText = text.getText();
        Pattern pattern = Pattern.compile("[A-Za-z\\s]*\\?");
        Matcher matcher = pattern.matcher(fullText);
        StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            sb.append(matcher.group()).append(" ");
        }
        pattern = Pattern.compile("\\b[A-Za-z0-9]\\w*");
        matcher = pattern.matcher(sb);
        Set<String> wordsByLength = new HashSet<>();
        while (matcher.find()) {
            String string = matcher.group();
            if (string.length() == length) {
                wordsByLength.add(string);
            }
        }
        return wordsByLength;
    }

    /**
     * @return - In each sentence of the text, where the first word begins in vowel
     * to the longest word in this sentence.
     */
    public List<String> fifthTask() {
        List<String> sen = new ArrayList<>();
        Pattern pattern = Pattern.compile("^[AOUEI]\\w*?\\s");
        for (Sentence sentence : text.getSentences()) {
            Matcher matcher = pattern.matcher(sentence.getSentence());
            if (matcher.find()) {
                List<Word> words = sentence.getWords();
                String biggest = words.stream()
                        .map(Word::getWord).max((a, b) -> a.length() > b.length() ? a.length() < b.length() ? 1 : -1 : 0)
                        .get();
                sen.add(matcher.replaceAll(biggest + " "));
            }
        }
        return sen;
    }

    /**
     * @return - the words of the text in alphabetical order in the first letter. Words,
     * beginning with a new letter, print with a new paragraph.
     */
    public List<String> sixthTask() {
        List<String> list = new ArrayList<>(getWordsSet());
        List<String> collect = list.stream()
                .sorted((a, b) -> Character.valueOf(a.charAt(0)).compareTo(b.charAt(0)))
                .collect(Collectors.toList());
        return collect;
    }

    /**
     * @return - sorted words of the text in ascending percentage of alphabetic letters
     * (the ratio of the number of vowels to the total number of letters in a word).
     */
    public List<Map.Entry<String, Double>> seventhTask() {
        Pattern pattern = Pattern.compile("[AEIOUaeiou]");
        Map<String, Double> map = new HashMap<>();
        for (String word : getWordsSet()) {
            Matcher matcher = pattern.matcher(word);
            int count = 0;
            while (matcher.find()) {
                count++;
            }
            double attitudeToVowels = (double) count / (double) word.length();
            map.put(word, attitudeToVowels);
        }
        List<Map.Entry<String, Double>> list = map.entrySet().stream()
                .sorted((a, b) -> a.getValue().compareTo(b.getValue()))
                .collect(Collectors.toList());
        return list;
    }

    /**
     * @return - the words of the text beginning with the vowel letters are sorted in
     * alphabetical order of the first letter of the word.
     */
    public List<String> eighthTask() {
        Pattern pattern = Pattern.compile("^[aeoiu]");
        Set<String> wordsSet = getWordsSet();
        List<String> wordsStartsVowels = new ArrayList<>();
        for (String s : wordsSet) {
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                wordsStartsVowels.add(s);
            }
        }
        Pattern pattern2 = Pattern.compile("[^aoiue]");
        List<String> sortedList = wordsStartsVowels.stream()
                .sorted((a, b) -> {
                    Matcher matcher = pattern2.matcher(a);
                    Character charA;
                    Character charB;
                    if (matcher.find()) {
                        charA = matcher.group().charAt(0);
                    } else {
                        charA = 'z';
                    }
                    matcher = pattern2.matcher(b);
                    if (matcher.find()) {
                        charB = matcher.group().charAt(0);
                    } else {
                        charB = 'z';
                    }
                    return charA.compareTo(charB);
                }).collect(Collectors.toList());
        return sortedList;
    }

    /**
     * @param c - given letter to sort words.
     * @return - all words in the text by increasing the number of letters given in
     * words. Arrange words with equal number in alphabetical order.
     */
    public List<Map.Entry<String, Integer>> ninthTask(char c) {
        Pattern pattern = Pattern.compile("[" + c + "]");
        Map<String, Integer> map = new HashMap<>();
        for (String word : getWordsSet()) {
            Matcher matcher = pattern.matcher(word);
            int count = 0;
            while (matcher.find()) {
                count++;
            }
            map.put(word, count);
        }
        List<Map.Entry<String, Integer>> sorted = map.entrySet().stream()
                .sorted((a, b) -> {
                    int i = b.getValue().compareTo(a.getValue());
                    if (i == 0) {
                        i = b.getKey().compareTo(a.getKey());
                    }
                    return i;
                })
                .collect(Collectors.toList());
        return sorted;
    }

    /**
     * @return - for each word in the list of words find how many
     * times it occurs in every sentence and sort the words by
     * decreasing total occurrences.
     */
    public List<Map.Entry<String, Integer>> tenthTask() {
        Set<String> wordsSet = getWordsSet();
        Map<String, Integer> map = new HashMap<>();
        for (String word : wordsSet) {
            Pattern pattern = Pattern.compile("[\\W]" + word + "[\\W]");
            int count = 0;
            for (Sentence sen : text.getSentences()) {
                Matcher matcher = pattern.matcher(sen.getSentence());
                if (matcher.find()) {
                    count++;
                }
            }
            map.put(word, count);
        }
        List<Map.Entry<String, Integer>> sorted = map.entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .collect(Collectors.toList());
        return sorted;
    }

    /**
     * @return - unique words from all the text in lowercase.
     */
    private Set<String> getWordsSet() {
        Set<String> wordsSet = new HashSet<>();
        for (Sentence s : text.getSentences()) {
            for (Word w : s.getWords()) {
                wordsSet.add(w.getWord().toLowerCase());
            }
        }
        return wordsSet;
    }

}
