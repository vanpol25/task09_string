package com.polahniuk.model;

/**
 * Class StringUtils with an undefined number of parameters of any class that
 * concatenates all parameters and returns Strings.
 */
public class StringUtils {

    private StringBuilder sb = new StringBuilder();
    private Object[] objects;

    /**
     * @param objects - any object like array.
     */
    public StringUtils(Object... objects) {
        this.objects = objects;
    }

    public void add(String string) {
        sb.append(string).append(" ");
    }

    /**
     * @return - StringBuilder of concatenates objects witch has override method toString.
     */
    public StringBuilder getConcatenates() {
        for (Object o : objects) {
            sb.append(o).append(" ");
        }
        return sb;
    }
}
