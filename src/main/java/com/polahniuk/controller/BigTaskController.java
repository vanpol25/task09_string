package com.polahniuk.controller;

import com.polahniuk.model.big_task.BigTask;
import com.polahniuk.model.big_task.Sentence;
import org.apache.logging.log4j.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Controller for big task.
 * Class has reference to text.txt {@link BigTaskController#REFERENCE_FILE}.
 * text.txt is main project file.
 */
public class BigTaskController {

    private Logger log = LogManager.getLogger(BigTaskController.class);
    private BigTask b = null;
    /**
     * Main project file.
     */
    private final static String REFERENCE_FILE = "C:\\Users\\Van_PC\\git\\task09_string\\string\\src\\main\\resources\\text.txt";

    public BigTaskController() {
        log.info("constructor");
        try {
            b = new BigTask(REFERENCE_FILE);
        } catch (IOException e) {
            log.info(e);
        }
    }

    public List<Map.Entry<String, Integer>> firstTask() {
        return b.firstTask();
    }

    public List<Map.Entry<Sentence, Integer>> secondTask() {
        return b.secondTask();
    }

    public String thirdTask() {
        return b.thirdTask();
    }

    public Set<String> fourthTask(int length) {
        return b.fourthTask(length);
    }

    public List<String> fifthTask() {
        return b.fifthTask();
    }

    public List<String> sixthTask() {
        return b.sixthTask();
    }

    public List<Map.Entry<String, Double>> seventhTask() {
        return b.seventhTask();
    }

    public List<String> eighthTask() {
        return b.eighthTask();
    }

    public List<Map.Entry<String, Integer>> ninthTask(char c) {
        return b.ninthTask(c);
    }

    public List<Map.Entry<String, Integer>> tenthTask() {
        return b.tenthTask();
    }

    public void showText() {
        System.out.println(b.getText());
    }
}
