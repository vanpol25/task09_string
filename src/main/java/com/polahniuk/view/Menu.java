package com.polahniuk.view;

import com.polahniuk.model.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Menu view of project which works with console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private static final String MENU_FILE_NAME = "Menu";
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private Locale locale;
    private ResourceBundle resourceBundle;
    private BigTaskMenu btm;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     * Also creates {@link Locale} and {@link ResourceBundle}.
     */
    public Menu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        btm = new BigTaskMenu();
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::testStringUtils);
        methods.put("2", this::internationalizeMenuUkrainian);
        methods.put("3", this::internationalizeMenuEnglish);
        methods.put("4", this::internationalizeMenuBelarusian);
        methods.put("5", this::internationalizeMenuRussian);
        methods.put("6", this::internationalizeMenuPolish);
        methods.put("7", this::testRegex);
        methods.put("8", this::testSplit);
        methods.put("9", this::testReplace);
        methods.put("0", this::bigTask);
        show();
    }

    /**
     * Set data from properties with field {@link Menu#locale}.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("9", resourceBundle.getString("9"));
        menu.put("0", resourceBundle.getString("0"));
        menu.put("Q", resourceBundle.getString("Q"));
    }

    /**
     * Shows work class {@link StringUtils}.
     */
    private void testStringUtils() {
        StringUtils su = new StringUtils("Hello", "world!", Integer.valueOf(2), Double.valueOf(4.5));
        StringBuilder text = su.getConcatenates();
        System.out.println("Test StringUtils, text = " + text);
        log.debug(text);
    }

    /**
     * Sets ukrainian locale to {@link Menu#locale} and show it in console.
     */
    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        show();
    }

    /**
     * Sets ukrainian locale to {@link Menu#locale} and show it in console.
     */
    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        show();
    }

    /**
     * Sets belarusian locale to {@link Menu#locale} and show it in console.
     */
    private void internationalizeMenuBelarusian() {
        locale = new Locale("be");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        show();
    }

    /**
     * Sets russian locale to {@link Menu#locale} and show it in console.
     */
    private void internationalizeMenuRussian() {
        locale = new Locale("ru");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        show();
    }

    /**
     * Sets polish locale to {@link Menu#locale} and show it in console.
     */
    private void internationalizeMenuPolish() {
        locale = new Locale("pl");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        show();
    }

    /**
     * Test a regular expression that checks a sentence to see that it begins with
     * a capital letter and ends with a period.
     */
    private void testRegex() {
        System.out.println("Enter text:");
        String text = sc.nextLine();
        Pattern pattern = Pattern.compile("[A-Z].*\\.");
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            System.out.println("Sentence begins with a capital letter and ends with a period.");
        } else {
            System.out.println("Does not match the pattern.");
        }
    }

    /**
     * Split entered string on the words "the" or "you".
     */
    private void testSplit() {
        System.out.println("Enter text:");
        String text = sc.nextLine();
        Pattern pattern = Pattern.compile("(the)|(you)");
        String[] words = pattern.split(text);
        if (words.length > 0) {
            System.out.println("Words as: ");
            for (String s : words) {
                System.out.println(s + " ");
            }
        } else {
            System.out.println("Does not match the pattern.");
        }
    }

    /**
     * Replacing all the vowels in entered text with underscores.
     */
    private void testReplace() {
        System.out.println("Enter text:");
        String text = sc.nextLine();
        Pattern pattern = Pattern.compile("[aoueiAOUEI]");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            System.out.println("Replaced: ");
            System.out.println(matcher.replaceAll("_"));
        } else {
            System.out.println("Does not match the pattern.");
        }
    }

    /**
     * Shows menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

    /**
     * Method shows menu of {@link BigTaskMenu}.
     */
    private void bigTask() {
        btm.show(locale);
    }

}
