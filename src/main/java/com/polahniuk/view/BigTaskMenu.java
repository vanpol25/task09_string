package com.polahniuk.view;

import com.polahniuk.controller.BigTaskController;
import com.polahniuk.model.big_task.Sentence;
import org.apache.logging.log4j.*;

import java.text.DecimalFormat;
import java.util.*;

/**
 * View of BigTask which works with console.
 */
public class BigTaskMenu {

    private Logger log = LogManager.getLogger(BigTaskMenu.class);
    private static final String MENU_FILE_NAME = "BigTaskMenu";
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private Locale locale;
    private ResourceBundle resourceBundle;
    private BigTaskController btc = new BigTaskController();

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     * Also creates {@link Locale} and {@link ResourceBundle}.
     */
    public BigTaskMenu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::firstTask);
        methods.put("2", this::secondTask);
        methods.put("3", this::thirdTask);
        methods.put("4", this::fourthTask);
        methods.put("5", this::fifthTask);
        methods.put("6", this::sixthTask);
        methods.put("7", this::seventhTask);
        methods.put("8", this::eighthTask);
        methods.put("9", this::ninthTask);
        methods.put("10", this::tenthTask);
        methods.put("0", this::showText);
    }

    /**
     * Sets local from {@link Menu}.
     * @param locale - {@link Locale} from {@link Menu}.
     */
    public void show(Locale locale) {
        this.locale = locale;
        resourceBundle = ResourceBundle.getBundle(MENU_FILE_NAME, locale);
        setMenu();
        show();
    }

    /**
     * Set data from properties with field {@link BigTaskMenu#locale}.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("9", resourceBundle.getString("9"));
        menu.put("10", resourceBundle.getString("10"));
        menu.put("0", resourceBundle.getString("0"));
        menu.put("Q", resourceBundle.getString("Q"));
    }

    /**
     * Shows work first task of BigTask.
     */
    private void firstTask() {
        List<Map.Entry<String, Integer>> list = btc.firstTask();
        if (list == null) {
            System.out.println("No such word");
        } else {
            System.out.println("Top 20 used words.");
            for (int i = list.size() - 20; i < list.size(); i++) {
                System.out.println("More popular word is - \"" +
                        list.get(i).getKey().toLowerCase() + "\" founded in " + list.get(i).getValue() + " sentences.");
            }
        }
    }

    /**
     * Shows work second task of BigTask.
     */
    private void secondTask() {
        for (Map.Entry<Sentence, Integer> entry : btc.secondTask()) {
            System.out.println(entry);
        }
    }

    /**
     * Shows work third task of BigTask.
     */
    private void thirdTask() {
        System.out.println("\"" + btc.thirdTask() + "\" - is an unique word");
    }

    /**
     * Shows work fourth task of BigTask.
     */
    private void fourthTask() {
        System.out.println("Enter length:");
        Integer length = sc.nextInt();
        System.out.println("Words with length " + length + " :");
        Set<String> words = btc.fourthTask(length);
        System.out.println(words);
    }

    /**
     * Shows work fifth task of BigTask.
     */
    private void fifthTask() {
        System.out.println("Replacement sentences:");
        List<String> list = btc.fifthTask();
        for (String s : list) {
            System.out.println(s);
        }
    }

    /**
     * Shows work sixth task of BigTask.
     */
    private void sixthTask() {
        List<String> list = btc.sixthTask();
        String str = " ";
        for (String s : list) {
            if (s.charAt(0) != str.charAt(0)) {
                str = s;
                System.out.println();
            }
            System.out.print(s + ", ");
        }
    }

    /**
     * Shows work seventh task of BigTask.
     */
    private void seventhTask() {
        List<Map.Entry<String, Double>> list = btc.seventhTask();
        DecimalFormat df = new DecimalFormat("#.##");
        for (Map.Entry<String, Double> e : list) {
            System.out.println("Word - \"" + e.getKey() + "\"" +
                    " has attitude all to vowels = " + df.format(e.getValue()));
        }
    }

    /**
     * Shows work eighth task of BigTask.
     */
    private void eighthTask() {
        System.out.println("Words sorted by first consonant letter: ");
        List<String> list = btc.eighthTask();
        for (String s : list) {
            System.out.println(s);
        }
    }

    /**
     * Shows work ninth task of BigTask.
     */
    private void ninthTask() {
        System.out.println("Enter character:");
        char c = sc.nextLine().charAt(0);
        List<Map.Entry<String, Integer>> list = btc.ninthTask(c);
        System.out.println("Sorted by the number of letter and alphabetically:");
        for (Map.Entry<String, Integer> e : list) {
            System.out.println(e.getKey());
        }
    }

    /**
     * Shows work tenth task of BigTask.
     */
    private void tenthTask() {
        List<Map.Entry<String, Integer>> entries = btc.tenthTask();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Word - \"" + entry.getKey() + "\" = " + entry.getValue());
        }
    }

    /**
     * Shows text of BigTask.
     */
    private void showText() {
        btc.showText();
    }

    /**
     * Shows menu of BigTask in console.
     */
    private void getMenu() {
        System.out.println("\nBIG TASK MENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    public void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}
